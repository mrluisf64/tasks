import Vue from "vue";
import App from "./App.vue";
import { routes } from "./routes";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueRouter from "vue-router";
import Vuelidate from "vuelidate";
import Notifications from "vue-notification";
import axios from "axios";
// Import Bootstrap an BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { getDataDecrypt } from "./helpers/encrypt";

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

Vue.use(VueRouter);

Vue.use(Vuelidate);

Vue.use(Notifications);

Vue.config.productionTip = false;

axios.defaults.baseURL = process.env.VUE_APP_API_URL;

axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

axios.interceptors.request.use(
  async config => {
    if (localStorage.session) {
      config.headers = {
        Token: getDataDecrypt("session")
      };
    }
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

const globalData = new Vue({
  data: { $appAuth: false }
});

Vue.mixin({
  computed: {
    $appAuth: {
      get: function() {
        return globalData.$data.$appAuth;
      },
      set: function(value) {
        globalData.$data.$appAuth = value;
      }
    }
  }
});

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes, // short for `routes: routes`
  mode: "history"
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
