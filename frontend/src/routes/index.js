import Task from "../components/task";
import Home from "../components/home";
import Profile from "../components/profile";

export const routes = [
  { path: "/", component: Home, name: "home" },
  { path: "/tasks", component: Task, name: "tasks.index" },
  { path: "/profile", component: Profile, name: "profile" },
];
