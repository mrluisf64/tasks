import axios from "axios";

class AuthService {
  signup(body) {
    return axios.post("auth/signup", body);
  }

  signin(body) {
    return axios.post("auth/signin", body);
  }

  profile() {
    return axios.get("auth/profile");
  }
}

export default new AuthService();
