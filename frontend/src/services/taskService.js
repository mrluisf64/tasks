import axios from "axios";

class TaskService {
  index() {
    return axios.get("tasks");
  }

  create(body) {
    return axios.post("tasks", body);
  }

  update(body) {
    return axios.put("tasks", body);
  }

  destroy(id) {
    return axios.delete("tasks/" + id);
  }
}

export default new TaskService();
