import express, { Application } from 'express'
import { createConnection } from 'typeorm'
import { pagination } from 'typeorm-pagination'
import authRoutes from './routes/auth'
import taskRoutes from './routes/task'
import morgan from 'morgan'
import User from './database/entities/user.entity'
import Task from './database/entities/task.entity'
import cors from 'cors'
import dotenv from 'dotenv'
dotenv.config()

const app: Application = express()

// database options
createConnection({
  name: 'default',
  type: 'postgres',
  host: process.env.DATABASE_HOST || '',
  port: 5432,
  username: process.env.DATABASE_USERNAME || '',
  password: process.env.DATABASE_PASSWORD || '',
  database: process.env.DATABASE_NAME || '',
  entities: [User, Task],
  logging: true,
  synchronize: true
})
  .then()
  .catch(e => console.log(e))

app.use(pagination)
// settings
app.set('port', 3000)

// middlewares
app.use(morgan('dev'))
app.use(express.json())
app.use(cors())

// routes
app.use('/api/auth', authRoutes)
app.use('/api/tasks', taskRoutes)

export default app
