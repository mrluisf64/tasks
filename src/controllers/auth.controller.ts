import User from '../database/entities/user.entity'
import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'

export async function encryptPassword (password: string): Promise<string> {
  const salt = await bcrypt.genSalt(10)
  return bcrypt.hash(password, salt)
}

function validatePassword (
  password: string,
  passwordHashed: string
): Promise<boolean> {
  return bcrypt.compare(password, passwordHashed)
}

class AuthController {
  async signup (req: Request, res: Response) {
    const repository = getRepository(User)
    const user = new User()
    user.name = req.body.name
    user.username = req.body.username
    user.password = await encryptPassword(req.body.password)
    const userSaved = await repository.save(user).catch(e => {
      return res.status(400).json({
        message: 'Ha ocurrido un error',
        body: e
      })
    })

    const token: string = jwt.sign(
      { id: user.id },
      process.env.TOKEN_SECRET || 'TOKENTEST',
      { expiresIn: 60 * 60 * 24 }
    )

    res.status(200).json({
      body: userSaved,
      session: token
    })
  }

  async signin (req: Request, res: Response) {
    const repository = getRepository(User)
    const userFind = await repository
      .findOne({
        username: req.body.username
      })
      .catch(e => {
        throw e
      })

    if (!userFind) return res.status(400).json('Username or password is wrong')

    const correctPassword: boolean = await validatePassword(
      req.body.password,
      userFind.password
    )

    if (!correctPassword) return res.status(400).json('Invalid password')

    const token: string = jwt.sign(
      { id: userFind.id },
      process.env.TOKEN_SECRET || 'TOKENTEST',
      { expiresIn: 60 * 60 * 24 }
    )

    res.header('token', token).json({
      body: userFind,
      session: token
    })
  }

  async profile (req: Request, res: Response) {
    const repository = getRepository(User)
    const user = await repository.findOne(req.userId)
    if (!user) return res.status(404).json('No user found')
    return res.json(user)
  }
}

export default new AuthController()
