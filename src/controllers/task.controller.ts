import Task from '../database/entities/task.entity'
import { getRepository } from 'typeorm'
import { Request, Response } from 'express'
import User from '../database/entities/user.entity'

class TaskController {
  async index (req: Request, res: Response) {
    const repository = getRepository(Task)
    const listTasks = await repository.find({
      where: { user: req.userId },
      relations: ['user']
    })

    res.send(listTasks)
  }

  async create (req: Request, res: Response) {
    const taskRepository = getRepository(Task)
    const userRepository = getRepository(User)

    const task = new Task()
    task.name = req.body.name
    task.description = req.body.description

    const user = await userRepository.findOne(req.userId)
    if (!user) return res.status(400).json('User no exists')

    task.user = user
    const taskSaved = await taskRepository.save(task)

    res.json({
      body: taskSaved,
      status: 200
    })
  }

  async update (req: Request, res: Response) {
    const repository = getRepository(Task)
    const taskUpdate = await repository.findOne(req.body.taskId)
    if (!taskUpdate) return res.status(400).json('Task no exists')
    taskUpdate.name = req.body.name ? req.body.name : taskUpdate.name
    taskUpdate.description = req.body.description
      ? req.body.description
      : taskUpdate.description
    taskUpdate.status =
      req.body.status != undefined ? req.body.status : taskUpdate.status
    const taskSaved: Task = await repository.save(taskUpdate)

    res.json({
      body: taskSaved,
      status: 200
    })
  }

  async destroy (req: Request, res: Response) {
    const repository = getRepository(Task)
    const taskDeleted = await repository.findOne(req.body.taskId)
    if (!taskDeleted) return res.status(400).json('Task no exists')

    repository.delete(taskDeleted)
    res.status(200).json({
      body: 'OK',
      status: 200
    })
  }
}

export default new TaskController()
