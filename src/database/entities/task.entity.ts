import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { BaseEntity } from './base.entity'
import User from './user.entity'

@Entity('tasks')
export default class Task extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({
    type: 'varchar',
    nullable: false
  })
  name: string

  @Column({
    type: 'varchar',
    nullable: false
  })
  description: string

  @Column({
    type: 'bool',
    nullable: false,
    default: false
  })
  status: boolean

  @ManyToOne(
    () => User,
    user => user.tasks
  )
  user: User
}
