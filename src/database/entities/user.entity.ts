import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { BaseEntity } from './base.entity'
import Task from './task.entity'

@Entity('users')
export default class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({
    type: 'varchar',
    nullable: false
  })
  name: string

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true
  })
  username: string

  @Column({
    type: 'varchar',
    nullable: false
  })
  password: string

  @OneToMany(
    () => Task,
    task => task.user
  )
  tasks: Task[]
}
