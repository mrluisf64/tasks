import Task from '../database/entities/task.entity'
import { Request, Response, NextFunction } from 'express'
import { getRepository } from 'typeorm'

export const VerifyOwnerTask = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.params.taskId) req.body.taskId = req.params.taskId

  if (!req.body.taskId) return res.status(401).json('Access denied')
  const repository = getRepository(Task)

  const task = await repository.findOne(req.body.taskId, {
    relations: ['user']
  })

  if (!task) return res.status(401).json('Access denied')

  if (task.user.id === req.userId) return next()

  return res.status(401).json('Access denied')
}
