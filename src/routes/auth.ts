import AuthController from '../controllers/auth.controller'
import { Router } from 'express'
import { TokenValidation } from '../libs/verifyToken'

const router: Router = Router()

router.post('/signup', AuthController.signup)
router.post('/signin', AuthController.signin)
router.get('/profile', TokenValidation, AuthController.profile)

export default router
