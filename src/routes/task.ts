import TaskController from '../controllers/task.controller'
import { Router } from 'express'
import { TokenValidation } from '../libs/verifyToken'
import { VerifyOwnerTask } from '../libs/verifyOwnerTask'

const router: Router = Router()

router.get('/', TokenValidation, TaskController.index)
router.post('/', TokenValidation, TaskController.create)
router.put('/', [TokenValidation, VerifyOwnerTask], TaskController.update)
router.delete(
  '/:taskId',
  [TokenValidation, VerifyOwnerTask],
  TaskController.destroy
)

export default router
