import request from 'supertest'
import app from '../app'
import connection from './connection'
import { getRepository } from 'typeorm'
import User from '../database/entities/user.entity'
import { encryptPassword } from '../controllers/auth.controller'

beforeAll(async () => {
  await connection.create()
})

afterAll(async () => {
  await connection.close()
})

beforeEach(async () => {
  await connection.clear()
})

describe('Test Auth Controller API', async () => {
  it('Request auth signup status 200 successfully', async () => {
    const { body, status } = await request(app)
      .post('/api/auth/signup')
      .send({
        name: 'test',
        username: 'testings',
        password: '123',
        createdAt: new Date(),
        updatedAt: null
      })

    expect(status).toBe(200)
    expect(body.body).toEqual({
      name: body.body.name,
      username: body.body.username,
      password: body.body.password,
      createdAt: body.body.createdAt,
      updatedAt: body.body.updatedAt,
      id: body.body.id
    })
    expect(body.session).toEqual(body.session)
  })

  it('Request auth signin status 200 successfully', async () => {
    const repository = getRepository(User)
    await repository.save({
      name: 'test',
      username: 'nesting',
      password: await encryptPassword('123')
    })

    const { body, status } = await request(app)
      .post('/api/auth/signin')
      .send({
        username: 'nesting',
        password: '123'
      })

    expect(status).toBe(200)
    expect(body.body).toEqual({
      name: body.body.name,
      username: body.body.username,
      password: body.body.password,
      createdAt: body.body.createdAt,
      updatedAt: body.body.updatedAt,
      id: body.body.id
    })
    expect(body.session).toEqual(body.session)
  })

  it('Request auth profile status 200 successfully', async () => {
    const repository = getRepository(User)
    await repository.save({
      name: 'test',
      username: 'nesting',
      password: await encryptPassword('123')
    })

    const response = await request(app)
      .post('/api/auth/signin')
      .send({
        username: 'nesting',
        password: '123'
      })

    const token = response.body.session

    const { body, status } = await request(app)
      .get('/api/auth/profile')
      .set('Token', token)
      .send()

    expect(status).toBe(200)
    expect(body).toEqual({
      name: body.name,
      username: body.username,
      password: body.password,
      createdAt: body.createdAt,
      updatedAt: body.updatedAt,
      id: body.id
    })
  })
})
