import Task from '../database/entities/task.entity'
import User from '../database/entities/user.entity'
import { createConnection, getConnection } from 'typeorm'

const connection = {
  async create () {
    await createConnection({
      name: 'default',
      type: 'postgres',
      host: process.env.DATABASE_HOST || '',
      port: 5432,
      username: process.env.DATABASE_USERNAME || '',
      password: process.env.DATABASE_PASSWORD || '',
      database: process.env.DATABASE_NAME_TEST || '',
      entities: [User, Task],
      logging: true,
      synchronize: true
    })
  },

  async close () {
    await getConnection().close()
  },

  async clear () {
    const connection = getConnection()
    const entities = connection.entityMetadatas

    entities.forEach(async entity => {
      const repository = connection.getRepository(entity.name)
      await repository.query(`DELETE FROM ${entity.tableName}`)
    })
  }
}
export default connection
